# README #

1/3スケールの富士通 FM TOWNS風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。

***

# 実機情報

## メーカ
- 富士通

## 発売時期
- FM TOWNS 1/2/1S/2S 1989年2月28日
- FM TOWNS 1F/2F/1H/2H 1989年11月7日
- FM TOWNS 10F/20F/40H/80H 1990年10月30日
- FM TOWNSII CX 1991年11月5日

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/FM_TOWNS)
- [富士通ミュージアム](https://www.fujitsu.com/jp/about/plus/museum/products/computer/personalcomputer/fmtowns.html)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_fmtowns/raw/1b8e6b087a13c9a6fc50a1b364b15157b9f214f1/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_fmtowns/raw/1b8e6b087a13c9a6fc50a1b364b15157b9f214f1/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_fmtowns/raw/1b8e6b087a13c9a6fc50a1b364b15157b9f214f1/ExampleImage.jpg)
